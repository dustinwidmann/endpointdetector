# This Python file uses the following encoding: utf-8

# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic -g python form.ui -o ui_form.py

DUMMY_MODE = True

import sys

from PySide6.QtCore import QTimer
from PySide6.QtCore import Qt
from PySide6.QtCore import QIODevice
from PySide6.QtWidgets import QApplication
from PySide6.QtWidgets import QMainWindow
from PySide6.QtGui import QPainter
from PySide6.QtCharts import QLineSeries
from PySide6.QtCharts import QChart
from PySide6.QtCharts import QValueAxis
from PySide6.QtSerialPort import QSerialPort

from ui_form import Ui_MainWindow
from __feature__ import snake_case

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        """
        Initialize the MainWindow, and well, pretty much everything else too.
        """
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.y = -1                     ## dummy variable for testing
        self.timepoint = -1             ## current measurement time
        self.timer_increment = 0.5      ## time increment between each reading [seconds]
        self.displayed_time = 10        ## number of seconds of data to display at any given time
        self.kept_duration = 5*60*60    ## 5 hours of data [seconds]
        self.signal_scale_min = -10     ## signal axis scale, min value
        self.signal_scale_max = 110     ## signal axis scale, max value
        self.derivative_scale_min = -5  ## derivative axis scale, min value
        self.derivative_scale_max = 5   ## derivative axis scale, max value
        self.averaging = 10             ## number of samples to use in boxcar averaging
        self.com_port_name = "COM3"     ## com port to read from for serial data
        self.b1 = 1                     #### from the calibration curves for the device
        self.m1 = 1                     #### these four values
        self.b2 = 1                     #### need to be set to
        self.m2 = 1                     #### something appropriate
        self.signal_color = Qt.blue
        self.derivative_color = Qt.red
        self.averaging_data = [0] * self.averaging
        self.averaging_index = 0

        self.serial_port = QSerialPort(self.com_port_name)
        self.serial_port.set_baud_rate(9600)
        if not self.serial_port.open(QIODevice.ReadOnly):
            if not DUMMY_MODE:
                print("Could not open com port, exiting now.")
        self.ui.lcd_number.set_palette(Qt.red)
        self.ui.lcd_number.set_minimum_height(32)

        self.initialize_plot()
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.update_timeout)
        self.update_timer.start(self.timer_increment*1000)
        self.cleanup_timer = QTimer()
        self.cleanup_timer.timeout.connect(self.cleanup_timeout)
        self.cleanup_timer.start(1000*60)

    def set_displayed_time(self,displayed_time):
        self.displayed_time = displayed_time

    def set_timer_increment(self,timer_increment):
        self.timer_increment = timer_increment

    def initialize_plot(self):
        """
        Initialize the plot
        """
        self.signal_series = QLineSeries()

        # Create the plot and data series
        self.chart = QChart()
        self.chart.set_title("Endpoint Detection")
        self.chart.legend().hide()

        # create the plot axes and attach them to the QChart
        self.bottom_axis = QValueAxis()
        self.bottom_axis.set_tick_count(11)
        self.left_axis = QValueAxis()
        self.right_axis = QValueAxis()
        self.left_axis.set_range(self.signal_scale_min,self.signal_scale_max)
        self.right_axis.set_range(self.derivative_scale_min,self.derivative_scale_max)
        self.bottom_axis.set_range(-1,1)
        self.chart.add_axis(self.bottom_axis,Qt.AlignBottom)
        self.chart.add_axis(self.left_axis,Qt.AlignLeft)
        self.chart.add_axis(self.right_axis,Qt.AlignRight)

        self.signal_series = QLineSeries()
        self.derivative_series = QLineSeries()
        self.chart.add_series(self.signal_series)
        self.chart.add_series(self.derivative_series)

        self.signal_series.attach_axis(self.bottom_axis)
        self.signal_series.attach_axis(self.left_axis)
        self.derivative_series.attach_axis(self.bottom_axis)
        self.derivative_series.attach_axis(self.right_axis)

        self.signal_series.set_color(self.signal_color)
        self.derivative_series.set_color(self.derivative_color)
        self.left_axis.set_line_pen_color(self.signal_series.pen().color())
        self.left_axis.set_labels_color(self.signal_color)
        self.right_axis.set_labels_color(self.derivative_color)
        self.right_axis.set_line_pen_color(self.derivative_series.pen().color())

        self.ui.chart_view.set_render_hint(QPainter.Antialiasing)
        self.ui.chart_view.set_chart(self.chart)

    def update_timeout(self):
        """
        update trigger received, read data from serial,
        process that data, and update the plot
        """
        self.timepoint = self.timepoint + self.timer_increment
        data = self.read_data()
        processed_data = self.process_data(data)
        self.update_plot(processed_data)

    def cleanup_timeout(self):
        """
        Clean up the collected data from time to time to prevent it from getting
        too big. Otherwise the program will leak memory and eventually crash.
        """
        signal_points = self.signal_series.points()
        remove_idx = -1
        for pidx in range(len(signal_points)-1,-1,-1):
            p = signal_points[pidx]
            if p.x() < self.timepoint - self.kept_duration:
                remove_idx = pidx
                break
        if remove_idx > 0:
            print(f"removing points from 0 to {remove_idx+1}")
            self.signal_series.remove_points(0,remove_idx+1)
            self.derivative_series.remove_points(0,remove_idx+1)

    def read_data(self):
        """
        read data from serial and then return it
        """
        if DUMMY_MODE:
            self.y += 1
            return self.y
        else:
            self.serial_port.write(b'a')
            data = int(self.serial_port.readLine(1000).toStdString().strip())
            return data

    def process_data(self,data):
        """
        process serial data reeived from detector
        """
        processed_data = 0
        derivative = 0
        if DUMMY_MODE:
            processed_data = data
        else:
            # m1, b1, m2, b2 are the slopes and y-intercepts of a calibration curve split into two segments.
            # the calibration curve is more accurate if a slope is calculated at small values
            # separately from the large values.
            # These numbers correct for two things:
            #                   * the resistor ratio in the custom PCB
            #                   * turning the ADC value (0-1024) into an actual voltage
            # These values can be found in settings_interface module and are extracted from direct measurements.

            # if the value from the ADC is less than 150 (on a scale of 0-1024 that's equivalent to about 10V).
            if data < 150:
                processed_data = (data - self.b1) / self.m1
            else:
                processed_data = (data - self.b2) / self.m2

        self.ui.lcd_number.display(processed_data)
        self.averaging_data[self.averaging_index] = processed_data
        averaged = sum(self.averaging_data) / self.averaging
        last_index = (self.averaging_index + 1 + self.averaging) % self.averaging
        derivative = (self.averaging_data[self.averaging_index] - self.averaging_data[last_index]) / (self.timer_increment*self.averaging)
        self.averaging_index = (self.averaging_index + 1) % self.averaging
        return (averaged,derivative)

    def update_plot(self,processed_data):
        """
        make changes to the plot given the new data from detector
        """
        xmin = self.timepoint - self.displayed_time
        xmax = self.timepoint
        self.signal_series.append(self.timepoint,processed_data[0])
        self.derivative_series.append(self.timepoint,processed_data[1])
        self.bottom_axis.set_range(xmin,xmax)

    def show_about(self):
        pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec())
